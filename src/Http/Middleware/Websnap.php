<?php


namespace ShipIT\LaravelWebsnap\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use ShipIT\LaravelWebsnap\Screenshot;

class Websnap
{
    /**
     * @var Screenshot
     */
    private Screenshot $screenshot;

    public function __construct(
        Screenshot $screenshot
    )
    {
        $this->screenshot = $screenshot;
    }

    public function handle($request, Closure $next)
    {
        if (!$this->isWebsnapRequest($request)) {
            return $this->screenshot();
        }

        return $next($request);
    }

    private function isWebsnapRequest(Request $request): bool
    {
        return $request->hasHeader('X-Websnap-Token');
    }

    private function screenshot(): Response
    {
        $response = $this->screenshot->make(
            URL::current()
        )->getRawResponse();

        return response(
            $response->body(),
            200,
            [
                'Content-Type'   => $response->header('Content-Type'),
                'Content-Length' => $response->header('Content-Length'),
                'Cache-Control'  => $response->header('Cache-Control')
            ]
        );
    }
}