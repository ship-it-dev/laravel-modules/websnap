<?php


namespace ShipIT\LaravelWebsnap;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;

class UrlFactory
{
    /**
     * @var Container
     */
    private Container $container;
    /**
     * @var Repository
     */
    private Repository $config;

    public function __construct(
        Repository $config,
        Container $container
    ) {
        $this->container = $container;
        $this->config    = $config;
    }

    // TODO - write test
    public function to(string $url, array $params = []): Url
    {
        return $this->container->make(Url::class, [
            'attributes' => array_merge(
                [
                    'url'   => $url,
                    'token' => $this->config->get('websnap.token')
                ],
                $params
            )
        ]);
    }
}
