<?php


namespace ShipIT\LaravelWebsnap\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use ShipIT\LaravelWebsnap\Http\Middleware\Websnap;
use ShipIT\LaravelWebsnap\UrlFactory;

class WebsnapServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/websnap.php',
            'websnap'
        );

        if (!$this->app->runningInConsole()) {
            $this->getRouter()
                ->aliasMiddleware('websnap', Websnap::class);
        }

    }

    private function getRouter(): Router
    {
        return $this->app->make(Router::class);
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(UrlFactory::class);
    }
}
