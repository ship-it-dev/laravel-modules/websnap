<?php


namespace ShipIT\LaravelWebsnap;

use Illuminate\Support\Fluent;

class Url extends Fluent
{
    public function __toString()
    {
        $attributes = [];

        foreach ($this->getAttributes() as $key => $value) {
            if ($value === null) {
                continue;
            }

            $attributes[$key] = $value;
        }

        return 'https://img.websnap.app/?' . http_build_query($attributes);
    }
}
