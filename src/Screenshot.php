<?php


namespace ShipIT\LaravelWebsnap;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class Screenshot
{
    private UrlFactory $urlFactory;

    private Response $rawResponse;

    public function __construct(
        UrlFactory $urlFactory
    ) {
        $this->urlFactory = $urlFactory;
    }

    // TODO - write test
    public function make(string $url, array $params = []): Screenshot
    {
        $this->rawResponse = Http::timeout(15)->get(
            $this->urlFactory->to($url, $params)
        )->throw();

        return $this;
    }

    public function getRawResponse(): Response
    {
        return $this->rawResponse;
    }
}
